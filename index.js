// Product Constructor
class Product {
	constructor(name,price,isActive = true){
		this.name = name;
		this.price = price;
		this.isActive = isActive;
	}


// Product Methods
	archive(){
		if(this.isActive){
			this.isActive = false
		}
		return this
	}
	// Product1.archive()


	updatePrice(newPrice){
		this.price = newPrice;
		return this
	}
	// Product1.updatePrice(15.50)



}

let Product1 = new Product("soap", 12.99)
let Product2 = new Product("shampoo", 50.25)






// Cart Constructor
class Cart {
	constructor(){
		this.contents = [];
		this.totalAmount = 0;

	}

// Cart Methods

	addToCart(product,quantity){
		this.contents.push({product:product,quantity:quantity})
		return this;

	}
	// Customer1.cart.addToCart(Product1,2)



	showCartContents(){
		return this.contents;

	}
	// Customer1.cart.addToCart(Product1,2).showCartContents()


	updateProductQuantity(product,newQuantity){
		this.contents.forEach(addedProduct => {
			if(addedProduct.product === product){
				addedProduct.quantity = newQuantity;
			}
		})
		return this;

	}
	// Customer1.cart.updateProductQuantity(Product1,5)


	clearCartContents(){
		this.contents = [];
		return this;
	}
	// Customer1.cart.updateProductQuantity(Product1,5).clearCartContents()


	computeTotal(){
		let total = 0;
		this.contents.forEach(product => {
			total += product.product.price * product.quantity;
		})
		this.totalAmount = total;
		return this;

	}
	// Customer1.cart.addToCart(Product1,2).computeTotal()



}






// Customer Constructor
class Customer {
	constructor(email){
		this.email = email;
		this.cart = new Cart;
		this.orders = [];

	}

// Customer Methods
	checkOut(){
        if(this.cart.contents.length>0){
            this.orders.push({products:this.cart.contents,totalAmount:this.cart.computeTotal().totalAmount});  
        }
        return this;


	}
	// Customer1.cart.addToCart(Product1,2).computeTotal().checkOut()


}

let Customer1 = new Customer("john@mail.com")

